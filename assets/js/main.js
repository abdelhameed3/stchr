/* Add active  class in home slider */
$(document).ready(function () {
  'use strict';

  $(".side-nav .control-side").click(function () {
    $(this).toggleClass("active");
    $('body').toggleClass("side-bar-icon");
  });
  $(".side-nav .control-side").click(function () {
    if (!$("body").hasClass("side-bar-icon")) {
      $(".main-panel").addClass("hide-phone");
    } else {
      $(".main-panel").removeClass("hide-phone");
    }
  });
  $('#accordionNav li a img').click(function () {
    if ($("body").hasClass("side-bar-icon")) {
      $("body").removeClass("side-bar-icon");
    }
  });
});
$(document).ready(function () {
  "use strict";
  function reStyle() {
    if ($(window).width() <= 770) {
      $('body').addClass("side-bar-icon");
    } else {
      $('body').removeClass("side-bar-icon");
    }
  }
  reStyle();
  $(window).resize(function () {
    reStyle();
  });

});

/* Add active To aide nav
======================== */
$(document).ready(function () {
  "use strict";

  $("#accordionNav li a").each(function () {
    var t = window.location.href.split(/[?#]/)[0];
    this.href == t && ($(this).addClass("active"), $(this).parent().parent().parent().parent().children('a').addClass("active"));
  })

  
});

/* Add active 
======================== */
$(document).ready(function () {
  "use strict";
  $('.productCard .setting > i').click(function (){
    $('.productCard .setting .option').removeClass('active');
    $(this).next().toggleClass('active');
  });

  $(window).click(function() {
        // $(".productCard .setting .option").removeClass("active");
    });

    $('.productCard .setting').click(function(event){
        // event.stopPropagation();
    });

});

$(document).ready(function () {
  "use strict";
  var sectionNum = 0;
  var productNum = 0;
  $('.addSection').click(function () {
    sectionNum += 1;

    $('#accordion').append(`<div class="card">
    <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapse`+ sectionNum + `">
            Section `+ sectionNum + `
            <i class="fas fa-angle-up"></i>
        </a>
    </div>
    <div id="collapse`+ sectionNum + `" class="collapse " data-parent="#accordion">
        <div class="card-body">
            <h3>New Section .</h3>
           <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="title[]" type="text" placeholder="Section Name">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="field1[]" type="text" placeholder="filed name 1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="field2[]" type="text" placeholder="filed name 2">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="field3[]" type="text" placeholder="filed name 3">
                            </div>
                        </div>
                    </div>
            <div class="addProduct modelAdd">
                <a href="#"> + Add Product</a>
            </div>
            <h3>New Product .</h3>
            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" placeholder="Product Name">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="file">
                                </div>
                            </div>
                            <div class="col-1    mb-5">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                                    <label class="custom-control-label" for="customCheck">percentage</label>
                                </div>    
                            </div>

                        </div>
        </div> 
    </div> 
</div> `);

    $('.addProduct').click(function () {
      productNum += 1;

      $(this).next().parent().append(`
      <div class="row">
      <div class="col-md-6">
          <div class="form-group">
              <input type="text" placeholder="Product Name">
          </div>
      </div>
      <div class="col-md-5">
          <div class="form-group">
              <input type="file">
          </div>
      </div>
      <div class="col-1   mb-5">
          <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
              <label class="custom-control-label" for="customCheck">percentage</label>
          </div>    
      </div>

  </div>`);
      $(this).stopPropagation();
    });
  });
});

$(document).ready(function () {
  var productNum=0;
  $('.addProduct1').click(function () {
    productNum += 1;

    $(this).next().parent().append(`
    <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" placeholder="Product Name">
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <input type="file">
        </div>
    </div>
    <div class="col-1    mb-5">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
            <label class="custom-control-label" for="customCheck">percentage</label>
        </div>    
    </div>

</div>`);
  });
});

$(".delete").click(function () {
    var url = $(this).attr('data-content');
    console.log(url);
    var parentDiv = $(this).parent().parent().parent().parent();
       // $('div.elm'+$(this).attr('id'));

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'DELETE',
                url: url
            }).done(function (data) {
                parentDiv.remove();
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
            });
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your imaginary file is safe :)',
                'error'
            )
        }
    });
});
//
// $(".delete").click(function (e) {
//     e.preventDefault();
//     var url = $(this).attr('href');
//     var blo =  $(this).parent().parent().parent().parent();
//     console.log(url);
//     swal({
//         title: "هل أنت متأكد من الحذف ؟",
//         text: "بمجرد حذفها ، لن تكون قادراً على استرداد هذا  مرة اخري!",
//         icon: "warning",
//         buttons: true,
//         dangerMode: true,
//
//     })
//   .then((willDelete) => {
//             if (willDelete) {
//                 $.ajax({
//                     url: url,
//                     data: "json",
//                 }).done(function (data) {
//                     console.log(data.success);
//                     if(data.success == true){
//
//                         blo.hide();
//                         swal('تم الحذف بنجاح','','success');
//                     }else{
//                         swal('فشل الحذف ','','error');
//                     }
//                 });
//             } else {
//                 swal(" تم الغاء عملية الحذف!");
//             }
//         });
//// });
//$('.slidernav').slick({
//    dots: false,
//    arrows:false,
//    infinite: false,
//    speed: 300,
//    slidesToShow: 3,
//    slidesToScroll: 3,
//    variableWidth: true
//});

//$('.slidernav .nav-link').click(function (){
//    $('.slidernav .nav-link').removeClass('active show');
//    $(this).addClass('active show');
//    var strTop =$(this).attr('href');
////    var finalStr = strTop.substring(1);
////    console.log(finalStr);
//    $('.tab-content #'+strTop+'').addClass()
//});

$('.addTabs .showAcc').click(function(){
    $('.accordionPos').toggleClass('active');
});