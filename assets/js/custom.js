$(document).ready(function () {
    var url = $('a.management');

    $(url).on('click', function () {
        $.ajax({
            url: $(this).attr('data-content')
        }).done(function (data) {
            $('ul.sections').empty().append(data.sections);
            $('div.head_inforamtion').empty().append(data.manager);

            var url2 = $('a.section');

            $(url2).on('click', function () {
                $.ajax({
                    url: $(this).attr('data-content')
                }).done(function (data2) {
                    $('div.products').empty().append(data2.products);
                });
            });
        });
    });

    $('a.editRecruitment').on('click', function () {
        var url = $(this).attr('data-content');
        var updateUrl = '/admin/recruitment/'+url.split('/')[5];

        $('form#jobForm').attr('action', updateUrl);

        $.ajax({
           url: url
        }).done(function (results) {
            $('form#jobForm input[name="name"]').attr('value', results.data.name);
            $('form#jobForm input[name="title"]').attr('value', results.data.title);
            $('form#jobForm input[name="salary"]').attr('value', results.data.salary);
            $('form#jobForm input[name="email"]').attr('value', results.data.email);
            $('form#jobForm textarea[name="about"]').text(results.data.about);

            var selectedStatus = $("form#jobForm select#status option[value="+results.data.status+"]");
            var selectedGender = $("form#jobForm select#gender option[value="+results.data.gender+"]");
            var selectedType = $("form#jobForm select#type option[value="+results.data.type+"]");
            var selectedGrad = $("form#jobForm select#grad option[value="+results.data.grad+"]");

            if(selectedStatus.length !== 0){
                selectedStatus.attr('selected', 'selected')
            }else{
                $("select#status").val("");
            }

            if(selectedGender.length !== 0){
                selectedGender.attr('selected', 'selected')
            }else{
                $("select#gender").val("");
            }

            if(selectedType.length !== 0){
                selectedType.attr('selected', 'selected')
            }else{
                $("select#type").val("");
            }

            if(selectedGrad.length !== 0){
                selectedGrad.attr('selected', 'selected')
            }else{
                $("select#grad").val("");
            }
        });
    });
});


var url = $('a.section');

$(url).on('click', function () {
    $.ajax({
        url: $(this).attr('data-content')
    }).done(function (data2) {
        $('div.products').empty().append(data2.products);
    });
});

var url = $('a.section_all');

$(url).on('click', function () {
    $.ajax({
        url: $(this).attr('data-content')
    }).done(function (data2) {
        $('div.products').empty().append(data2.products);
    });
});

$('#gradFilter').on('change', function () {
    var grad = $(this).val();

    $.ajax({
        url: '/admin/getRecruitmentByGrad/'+grad
    }).done(function (data) {
        $('#internal').html(data.internal_recruitment);
        $('#external_quota').html(data.external_quota_recruitment);
        $('#external_out_quota').html(data.external_out_quota_recruitment);
    })
});
